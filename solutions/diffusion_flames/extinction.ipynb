{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c75e0c39-8fd6-4080-90f6-8240636c07c9",
   "metadata": {},
   "source": [
    "# Non-premixed (diffusion) flame: study of the effect of varying inflow velocities on the H2-air diffusion flame structure.\n",
    "An opposed-jet burner is used to study the effect of varying inflow velocities on the H2-air diffusion flame structure. Pure H2 enters from the nozzle at x = 0 cm, and air from the nozzle at x = 1.0 cm. Both streams are at T = 300K, and the velocity magnitude, v, at both nozzles is the same. \n",
    "\n",
    "**Exercise:** Keeping the velocity magnitude at the two nozzles equal, increase it in appropriate\n",
    "steps in order to find the value for which the flame extinguishes to within 50 cm/s.\n",
    "Estimate the strain rate as $a = \\frac{|U_f|+|U_{ox}|}{H}$, where $H$ is the distance between the nozzles, and plot the maximum flame temperature as a function of $1/a$. Discuss the result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6accf5eb-727a-44c8-924d-6bdef01f1291",
   "metadata": {},
   "outputs": [],
   "source": [
    "import copy\n",
    "import cantera as ct\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.lines import Line2D\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf590bdf-bb0f-4099-8a40-f03f6ad0689d",
   "metadata": {},
   "source": [
    "Let's first create a useful function that compute steady state solution of a diffusion flame based on the size of the domain and flow properties at boundary conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24a356ef-00fd-40e9-b134-f9161db0ee36",
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_1dcounterflow(gas, width, mdot_f, comp_f, tin_f, mdot_o, comp_o, tin_o, loglevel=1):\n",
    "    \"\"\"\n",
    "    Counter-flow Diffusion Flame from cantera\n",
    "    \"\"\"\n",
    "\n",
    "    # diffusion flame object\n",
    "    f = ct.CounterflowDiffusionFlame(gas, width=width)\n",
    "\n",
    "    # inlet fuel\n",
    "    f.fuel_inlet.mdot = mdot_f\n",
    "    f.fuel_inlet.X = comp_f\n",
    "    f.fuel_inlet.T = tin_f\n",
    "    \n",
    "    # inlet oxidizer\n",
    "    f.oxidizer_inlet.mdot = mdot_o\n",
    "    f.oxidizer_inlet.X = comp_o\n",
    "    f.oxidizer_inlet.T = tin_o\n",
    "    \n",
    "    # grid criteria\n",
    "    f.set_refine_criteria(ratio=4, slope=0.2, curve=0.3, prune=0.04)\n",
    "    \n",
    "    # solve the problem\n",
    "    f.solve(loglevel, refine_grid=True, auto=True)\n",
    "\n",
    "    assert f.grid[-1] == WIDTH # adaptative grid may change the width\n",
    "\n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b128634-4801-4e0e-86e7-30b2e23391b9",
   "metadata": {},
   "source": [
    "Let's define fixed parameters such as pressure and compositions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8567a078-c368-4c1b-80d5-7d3c9d6d5d04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input parameters\n",
    "PRESS = 101325.0             # pressure\n",
    "TIN_F = 300.0                # fuel inlet temperature\n",
    "COMP_F = 'H2:1'              # fuel composition\n",
    "TIN_OX = 300.0               # oxidizer inlet temperature\n",
    "COMP_OX = 'O2:1.0, N2:3.76'  # air composition\n",
    "WIDTH = 0.01                 # Distance between inlets is 1 cm\n",
    "loglevel = 1                 # amount of diagnostic output (0 to 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd86469f-459a-4d30-8b8f-3924402ae060",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the gas object used to evaluate all thermodynamic, kinetic, and\n",
    "# transport properties.\n",
    "gas = ct.Solution('h2o2.yaml')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f359d6a0-8cf3-466b-aa43-94b3ae4b364f",
   "metadata": {},
   "source": [
    "Gas density value at both sides is required for the computation of the mass flows. The Cantera's counter flow diffusion flame takes mass flow as input at the boundaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "746fc0a2-b31e-48a0-b2fb-0e0dc0ffe17b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute fuel density\n",
    "gas.TPX = TIN_F, PRESS, COMP_F\n",
    "RHO_F = copy.deepcopy(gas.density)\n",
    "\n",
    "# compute oxidizer density\n",
    "gas.TPX = TIN_OX, PRESS, COMP_OX\n",
    "RHO_OX = copy.deepcopy(gas.density)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6b285a-6a2a-467b-a33d-0987251cc8b4",
   "metadata": {},
   "source": [
    "We then increase the velocity by 0.5 m/s starting at 5 m/s for 150 iterations in order to find the extinction limit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96cd5d5d-fc04-441d-8cc8-154963c7d953",
   "metadata": {},
   "outputs": [],
   "source": [
    "velocity = 5. # initial velocity [m/s]\n",
    "dv = 0.5      # velocity step [m/s]\n",
    "N = 150       # number of iterations [-]\n",
    "velocity_arr = np.zeros(N)\n",
    "tempe_max_arr = np.zeros(N)\n",
    "for i in range(0, N):\n",
    "    # mass flows\n",
    "    mdot_f = velocity * RHO_F\n",
    "    mdot_o = velocity * RHO_OX\n",
    "    \n",
    "    # solver\n",
    "    f = solve_1dcounterflow(gas, WIDTH, mdot_f, COMP_F, TIN_F, mdot_o, COMP_OX, TIN_OX, loglevel=0)\n",
    "\n",
    "    # store velocity and maximum temperature\n",
    "    velocity_arr[i] = velocity\n",
    "    tempe_max_arr[i] = f.T.max()\n",
    "\n",
    "    if i%10==0:\n",
    "        print('Flame no. %d/%d, strain = %.1f s-1, max(T) = %.1f K' % (i+1, N, (velocity+velocity)/WIDTH, f.T.max()))\n",
    "\n",
    "    # velocity step\n",
    "    velocity += dv\n",
    "\n",
    "# compute strain rates\n",
    "strain_arr = np.divide(np.add(velocity_arr, velocity_arr), WIDTH)\n",
    "\n",
    "print('Done!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d994e855-ed57-42c7-a2e4-67df38cd024b",
   "metadata": {},
   "source": [
    "Let's compute the maximum strain rate before extinction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "749bef0f-8c45-4665-9c6f-eabf07f578c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "indices = np.where(tempe_max_arr >= (TIN_OX + 100.))[0]\n",
    "strain_lim = strain_arr[indices].max()\n",
    "print(' Maximum strain rate before extinction = %.1f s-1' % (strain_lim))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dc414f6-baae-4985-b44d-8f26a5adaf0f",
   "metadata": {},
   "source": [
    "Plot of the maximum temperature as a function of $1/a$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3933a4f0-97a0-4405-b7ec-58ca474d6378",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(ncols=2, figsize=(8.,3.))\n",
    "\n",
    "ax[0].plot(np.divide(1., strain_arr), tempe_max_arr, c='k')\n",
    "ax[1].plot(np.divide(1., strain_arr), tempe_max_arr, c='k', marker='+')\n",
    "\n",
    "for axx in ax:\n",
    "    axx.set_xlabel(r'$1/a$ [s]')\n",
    "    axx.set_ylabel(r'Max. temp. in the domain [K]')\n",
    "\n",
    "ax[0].set_ylim([TIN_OX, 2100])\n",
    "ax[1].set_ylim([TIN_OX, 1500])\n",
    "ax[1].set_xlim([1/(strain_lim+500), 1/(strain_lim-1500)])\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92531039-d381-4078-be38-63c92020700a",
   "metadata": {},
   "source": [
    "*Proposed discussion: The flame extinguishes for a strain rate ≈ 12600 s-1. The curve corresponds to the strongly-burning diffusion flame, which coexists with the extinguished (pure mixing) state for a range of strain rates. The complete diagram containing both solutions has the familiar S-shaped curves often encountered in combustion.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f3a618f-20ea-44c9-89de-1f7a25e3253a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
