{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c75e0c39-8fd6-4080-90f6-8240636c07c9",
   "metadata": {},
   "source": [
    "# Non-premixed (diffusion) flame: study of the effect of varying inflow velocities on the H2-air diffusion flame structure.\n",
    "An opposed-jet burner is used to study the effect of varying inflow velocities on the H2-air diffusion flame structure. Pure H2 enters from the nozzle at x = 0 cm, and air from the nozzle at x = 1.0 cm. Both streams are at T = 300K, and the velocity magnitude, v, at both nozzles is the same. \n",
    "\n",
    "**Exercise:** Compute the structure of the counterflow diffusion flame for v=100 cm/s, 200 cm/s, and 500 cm/s."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6accf5eb-727a-44c8-924d-6bdef01f1291",
   "metadata": {},
   "outputs": [],
   "source": [
    "import copy\n",
    "import cantera as ct\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.lines import Line2D"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf590bdf-bb0f-4099-8a40-f03f6ad0689d",
   "metadata": {},
   "source": [
    "Let's first create a useful function that compute steady state solution of a diffusion flame based on the size of the domain and flow properties at boundary conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24a356ef-00fd-40e9-b134-f9161db0ee36",
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_1dcounterflow(gas, width, mdot_f, comp_f, tin_f, mdot_o, comp_o, tin_o, loglevel=1):\n",
    "    \"\"\"\n",
    "    Counter-flow Diffusion Flame from cantera\n",
    "    \"\"\"\n",
    "\n",
    "    # diffusion flame object\n",
    "    f = ct.CounterflowDiffusionFlame(gas, width=width)\n",
    "\n",
    "    # inlet fuel\n",
    "    f.fuel_inlet.mdot = mdot_f\n",
    "    f.fuel_inlet.X = comp_f\n",
    "    f.fuel_inlet.T = tin_f\n",
    "    \n",
    "    # inlet oxidizer\n",
    "    f.oxidizer_inlet.mdot = mdot_o\n",
    "    f.oxidizer_inlet.X = comp_o\n",
    "    f.oxidizer_inlet.T = tin_o\n",
    "    \n",
    "    # grid criteria\n",
    "    f.set_refine_criteria(ratio=4, slope=0.2, curve=0.3, prune=0.04)\n",
    "    \n",
    "    # solve the problem\n",
    "    f.solve(loglevel, refine_grid=True, auto=True)\n",
    "\n",
    "    assert f.grid[-1] == WIDTH # adaptative grid may change the width\n",
    "\n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b128634-4801-4e0e-86e7-30b2e23391b9",
   "metadata": {},
   "source": [
    "Let's define fixed parameters such as pressure and compositions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8567a078-c368-4c1b-80d5-7d3c9d6d5d04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input parameters\n",
    "PRESS = 101325.0             # pressure\n",
    "TIN_F = 300.0                # fuel inlet temperature\n",
    "COMP_F = 'H2:1'              # fuel composition\n",
    "TIN_OX = 300.0               # oxidizer inlet temperature\n",
    "COMP_OX = 'O2:1.0, N2:3.76'  # air composition\n",
    "WIDTH = 0.01                 # Distance between inlets is 1 cm\n",
    "loglevel = 1                 # amount of diagnostic output (0 to 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd86469f-459a-4d30-8b8f-3924402ae060",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the gas object used to evaluate all thermodynamic, kinetic, and\n",
    "# transport properties.\n",
    "gas = ct.Solution('h2o2.yaml')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f359d6a0-8cf3-466b-aa43-94b3ae4b364f",
   "metadata": {},
   "source": [
    "Gas density value at both sides is required for the computation of the mass flows. The Cantera's counter flow diffusion flame takes mass flow as input at the boundaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "746fc0a2-b31e-48a0-b2fb-0e0dc0ffe17b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute fuel density\n",
    "gas.TPX = TIN_F, PRESS, COMP_F\n",
    "RHO_F = copy.deepcopy(gas.density)\n",
    "\n",
    "# compute oxidizer density\n",
    "gas.TPX = TIN_OX, PRESS, COMP_OX\n",
    "RHO_OX = copy.deepcopy(gas.density)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6b285a-6a2a-467b-a33d-0987251cc8b4",
   "metadata": {},
   "source": [
    "We then loop over three speed values, plotting the important quantities for comparison."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96cd5d5d-fc04-441d-8cc8-154963c7d953",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(10.,3.))\n",
    "\n",
    "ls = ['-', '-.', ':']\n",
    "velocity_arr = [1.0, 2.0, 5.0]\n",
    "for i, velocity in enumerate(velocity_arr):\n",
    "    \n",
    "    # mass flows\n",
    "    mdot_f = velocity * RHO_F\n",
    "    mdot_o = velocity * RHO_OX\n",
    "    \n",
    "    # solver\n",
    "    f = solve_1dcounterflow(gas, WIDTH, mdot_f, COMP_F, TIN_F, mdot_o, COMP_OX, TIN_OX)\n",
    "    \n",
    "    # Plot Temperature\n",
    "    ax[0].plot(f.flame.grid, f.T, ls=ls[i], c='k', label='%.0f m/s' % velocity)\n",
    "    \n",
    "    # Plot velocity\n",
    "    ax[1].plot(f.flame.grid, f.velocity, ls=ls[i], c='k')\n",
    "        \n",
    "    # Plot species\n",
    "    ax[2].plot(f.flame.grid, f.Y[gas.species_index('H2'), :], ls=ls[i], c='k')\n",
    "    ax[2].plot(f.flame.grid, f.Y[gas.species_index('OH'), :]*1e2, ls=ls[i], c='r')\n",
    "    ax[2].plot(f.flame.grid, f.Y[gas.species_index('O2'), :], ls=ls[i], c='g')\n",
    "        \n",
    "    \n",
    "for axx in ax:\n",
    "    axx.set_xlabel('Axis [m]')\n",
    "    \n",
    "ax[0].set_ylabel('Temperature [K]')\n",
    "ax[1].set_ylabel('Axial velocity [m/s]')\n",
    "ax[2].set_ylabel('Mass fraction [-]')\n",
    "    \n",
    "ax[0].legend()\n",
    "\n",
    "custom_lines = [Line2D([0], [0], color='k'),\n",
    "                Line2D([0], [0], color='r'),\n",
    "                Line2D([0], [0], color='g')]\n",
    "ax[2].legend(custom_lines, [r'$\\mathrm{H_2}$', r'OH$\\cdot 10^2$', r'$\\mathrm{O_2}$'])\n",
    "\n",
    "fig.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61b5d244-4774-4956-a479-7afd905e1308",
   "metadata": {},
   "source": [
    "## Physical analysis of the results\n",
    "\n",
    "Question: The stagnation point is the point where the velocity becomes zero. Closer to which nozzle is it located for the three cases considered? Why?\n",
    "\n",
    "*Proposed answer: Because of the higher density (and therefore higher momentum) of the oxidizer stream, the stagnation point where the momenta of the fuel and oxidizer streams become equal is located closer to the fuel nozzle.*\n",
    "\n",
    "Question: Which side of the stagnation point is the maximum temperature (Tmax) located and what happens to Tmax as the fuel and oxidizer velocities are increased? Why?\n",
    "\n",
    "*Proposed answer: Due to the high diffusivity of H2, the fuel diffuses through the stagnation poing and the maximum temperature point is located at the oxidizer side with respect to the stagnation point. As the inflow velocity, and therefore, the strain rate, is increased, the reaction zone becomes thinner and Tmax decreases. This is due mainly to the shorter residence time in the flame. In addition, the increased velocities result in steeper temperature gradients and therefore higher heat losses, further decreasing Tmax, whose location shifts towards the fuel together with the stagnation point.*\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82458416-e298-4701-a3a1-c7fddd9bbe1e",
   "metadata": {},
   "source": [
    "In the same plot show the profiles of the minor species for v = 100 and 500 cm/s. Comment on the location of the peaks of the minor species relative to the location of Tmax."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1dfd6b1b-bbf9-4917-9e51-cdd4363630ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6.,4.))\n",
    "axtw = ax.twinx()\n",
    "\n",
    "ls = ['-', '-.']\n",
    "velocity_arr = [1.0, 5.0]\n",
    "for i, velocity in enumerate(velocity_arr):\n",
    "    \n",
    "    # mass flows\n",
    "    mdot_f = velocity * RHO_F\n",
    "    mdot_o = velocity * RHO_OX\n",
    "    \n",
    "    # solver\n",
    "    f = solve_1dcounterflow(gas, WIDTH, mdot_f, COMP_F, TIN_F, mdot_o, COMP_OX, TIN_OX, loglevel=0)\n",
    "\n",
    "    # Plot Temperature\n",
    "    ax.plot(f.flame.grid, f.T, ls=ls[i], c='k', label='%.0f m/s' % velocity)\n",
    "\n",
    "    # Plot minor species\n",
    "    axtw.plot(f.flame.grid, f.Y[gas.species_index('H'), :]*1e1, ls=ls[i], c='b')\n",
    "    axtw.plot(f.flame.grid, f.Y[gas.species_index('OH'), :], ls=ls[i], c='r')\n",
    "    axtw.plot(f.flame.grid, f.Y[gas.species_index('HO2'), :]*1e2, ls=ls[i], c='m')\n",
    "        \n",
    "ax.set_xlabel('Axis [m]')\n",
    "ax.set_ylabel('Temperature [K]')\n",
    "axtw.set_ylabel('Mass fraction [-]')\n",
    "\n",
    "ax.set_xlim([0, 0.006])\n",
    "ax.set_ylim([0, 2400])\n",
    "axtw.set_ylim([0, 0.04])\n",
    "    \n",
    "ax.legend(loc='upper left')\n",
    "\n",
    "custom_lines = [Line2D([0], [0], color='b'),\n",
    "                Line2D([0], [0], color='r'),\n",
    "                Line2D([0], [0], color='m')]\n",
    "axtw.legend(custom_lines, [r'$\\mathrm{H} \\cdot 10^1$', r'OH', r'$\\mathrm{HO_2} \\cdot 10^2$'], loc='upper right')\n",
    "\n",
    "fig.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44237b39-3d0e-4980-91c9-ade5d687ac3d",
   "metadata": {},
   "source": [
    "*Proposed discussion: As it can be seen, OH peaks at locations close to the maximum temperature, slightly to the air side of Tmax. Because of its relatively high concentration and the fact that it can be more easily detected, it is measured by laser diagnostic techniques (Planar Laser Induced Fluorescence-PLIF) and used as a marker of the reaction zone. As expected from the kinetics of hydrogen/oxygen, YHO2 peaks in the lower temperature region, closer to the air nozzle where there is more oxygen. H atom peaks on the fuel side of Tmax due to the increased concentration of H2 from which it is produced. As the inflow velocity increases, the concentration of OH decreases while the mass fractions of the rest of the radicals increases.*"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
