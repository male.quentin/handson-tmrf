{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c75e0c39-8fd6-4080-90f6-8240636c07c9",
   "metadata": {},
   "source": [
    "# Non-premixed (diffusion) flame: study of the effect of varying inflow velocities on the H2-air diffusion flame structure.\n",
    "An opposed-jet burner is used to study the effect of varying inflow velocities on the H2-air diffusion flame structure. Pure H2 enters from the nozzle at x = 0 cm, and air from the nozzle at x = 1.0 cm. Both streams are at T = 300K, and the velocity magnitude, v, at both nozzles is the same. \n",
    "\n",
    "**Exercise:** Compute the structure of counterflow diffusion flames for v=100 cm/s and v=500 cm/s. Plot the species and temperature profiles in mixture fraction space. Define the mixture fraction $z_\\mathrm{H}$ in terms of the mass fraction of element H, and find the flame location in mixture fraction space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6accf5eb-727a-44c8-924d-6bdef01f1291",
   "metadata": {},
   "outputs": [],
   "source": [
    "import copy\n",
    "import cantera as ct\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.lines import Line2D\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf590bdf-bb0f-4099-8a40-f03f6ad0689d",
   "metadata": {},
   "source": [
    "Let's first create a useful function that compute steady state solution of a diffusion flame based on the size of the domain and flow properties at boundary conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24a356ef-00fd-40e9-b134-f9161db0ee36",
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_1dcounterflow(gas, width, mdot_f, comp_f, tin_f, mdot_o, comp_o, tin_o, loglevel=1):\n",
    "    \"\"\"\n",
    "    Counter-flow Diffusion Flame from cantera\n",
    "    \"\"\"\n",
    "\n",
    "    # diffusion flame object\n",
    "    f = ct.CounterflowDiffusionFlame(gas, width=width)\n",
    "\n",
    "    # inlet fuel\n",
    "    f.fuel_inlet.mdot = mdot_f\n",
    "    f.fuel_inlet.X = comp_f\n",
    "    f.fuel_inlet.T = tin_f\n",
    "    \n",
    "    # inlet oxidizer\n",
    "    f.oxidizer_inlet.mdot = mdot_o\n",
    "    f.oxidizer_inlet.X = comp_o\n",
    "    f.oxidizer_inlet.T = tin_o\n",
    "    \n",
    "    # grid criteria\n",
    "    f.set_refine_criteria(ratio=4, slope=0.2, curve=0.3, prune=0.04)\n",
    "    \n",
    "    # solve the problem\n",
    "    f.solve(loglevel, refine_grid=True, auto=True)\n",
    "\n",
    "    assert f.grid[-1] == WIDTH # adaptative grid may change the width\n",
    "\n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b128634-4801-4e0e-86e7-30b2e23391b9",
   "metadata": {},
   "source": [
    "Let's define fixed parameters such as pressure and compositions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8567a078-c368-4c1b-80d5-7d3c9d6d5d04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input parameters\n",
    "PRESS = 101325.0             # pressure\n",
    "TIN_F = 300.0                # fuel inlet temperature\n",
    "COMP_F = 'H2:1'              # fuel composition\n",
    "TIN_OX = 300.0               # oxidizer inlet temperature\n",
    "COMP_OX = 'O2:1.0, N2:3.76'  # air composition\n",
    "WIDTH = 0.01                 # Distance between inlets is 1 cm\n",
    "loglevel = 1                 # amount of diagnostic output (0 to 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd86469f-459a-4d30-8b8f-3924402ae060",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the gas object used to evaluate all thermodynamic, kinetic, and\n",
    "# transport properties.\n",
    "gas = ct.Solution('h2o2.yaml')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f359d6a0-8cf3-466b-aa43-94b3ae4b364f",
   "metadata": {},
   "source": [
    "Gas density value at both sides is required for the computation of the mass flows. The Cantera's counter flow diffusion flame takes mass flow as input at the boundaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "746fc0a2-b31e-48a0-b2fb-0e0dc0ffe17b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute fuel density\n",
    "gas.TPX = TIN_F, PRESS, COMP_F\n",
    "RHO_F = copy.deepcopy(gas.density)\n",
    "\n",
    "# compute oxidizer density\n",
    "gas.TPX = TIN_OX, PRESS, COMP_OX\n",
    "RHO_OX = copy.deepcopy(gas.density)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6b285a-6a2a-467b-a33d-0987251cc8b4",
   "metadata": {},
   "source": [
    "Let's compute the flame for v = 100 cm/s and v = 500 cm/s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96cd5d5d-fc04-441d-8cc8-154963c7d953",
   "metadata": {},
   "outputs": [],
   "source": [
    "f_list = []\n",
    "v_list = [1., 5.]\n",
    "for velocity in v_list: # velocity speed at the boundaries [m/s]\n",
    "    \n",
    "    # mass flows\n",
    "    mdot_f = velocity * RHO_F\n",
    "    mdot_o = velocity * RHO_OX\n",
    "    \n",
    "    # solver\n",
    "    f_list.append(solve_1dcounterflow(gas, WIDTH, mdot_f, COMP_F, TIN_F, mdot_o, COMP_OX, TIN_OX, loglevel=0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cb27b51-4a15-4e8a-8280-17d7612fc463",
   "metadata": {},
   "source": [
    "Now we need to compute the mixture fraction for each state within the flame. Let's first define the mixture fraction:\n",
    "$$ z_H=\\frac{Z_H-Z_H^{O x}}{Z_H^F-Z_H^{O x}} $$\n",
    "where $Z_H$ is the elemental mass fraction of H defined as\n",
    "$$ Z_H=W_H \\sum_{k=1}^N a_{k, H} \\frac{Y_k}{W_k} $$\n",
    "where $a_{k, H}$ is the number of H atoms in species $k$, and $W_k$ is the molecular weight of the species $k$ and $Y_k$ the mass fraction of the species $k$.\n",
    "For pure hydrogen as fuel and pure air as oxidizer, the elemental mass fraction of H in the oxidizer stream is $Z_H^{O x}=0$ and in the fuel stream $Z_H^{F}=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40830e59-bac6-41d8-9df7-aed7d0c9c00f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_mixture_fraction(gas):\n",
    "    z = (gas.elemental_mass_fraction('H') - 0.)/(1. - 0.)\n",
    "    return z"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a79b0428-bdb2-4ae5-acdc-4a3ac6602442",
   "metadata": {},
   "source": [
    "Then, we loop throught the flame grid points computing $z_\\mathrm{H}$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63c60415-d8ad-4874-ae22-600750d5fc1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "z_flames = []\n",
    "for f in f_list:\n",
    "    z = np.zeros(len(f.flame.grid))\n",
    "    for i in range(0, len(f.flame.grid)):\n",
    "        gas.TPY = f.T[i], PRESS, f.Y[:, i]\n",
    "        z[i] = get_mixture_fraction(gas)\n",
    "    z_flames.append(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7702a442-df1a-4b23-b097-bf8b16eb2f48",
   "metadata": {},
   "source": [
    "We will now compute the stoichiometric mixture fraction $z_{st}$ to plot its location:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01640e47-4dc5-4c04-a4b9-12e9af576ba4",
   "metadata": {},
   "outputs": [],
   "source": [
    "gas.TP = 300, PRESS\n",
    "gas.set_equivalence_ratio(phi=1.0, fuel=COMP_F, oxidizer=COMP_OX)\n",
    "z_st = get_mixture_fraction(gas)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5bb47dc-0f3b-47e9-a808-5e03b044f087",
   "metadata": {},
   "source": [
    "Now, let's plot the flames in mixture fraction space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f45dd0dd-6a01-4321-bf62-9731ad69118c",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls = ['-', ':']\n",
    "fig, ax = plt.subplots(ncols=2, figsize=(9,3.5))\n",
    "\n",
    "for i, f in enumerate(f_list):\n",
    "    \n",
    "    # plot temperature\n",
    "    ax[0].plot(z_flames[i], f.T, c='k', ls=ls[i], label='%.0f m/s' % v_list[i])\n",
    "\n",
    "    # plot species\n",
    "    ax[1].plot(z_flames[i], f.Y[gas.species_index('H'), :]*1e1, c='c', ls=ls[i])\n",
    "    ax[1].plot(z_flames[i], f.Y[gas.species_index('OH'), :], c='r', ls=ls[i])\n",
    "    ax[1].plot(z_flames[i], f.Y[gas.species_index('HO2'), :]*1e2, c='m', ls=ls[i])\n",
    "\n",
    "for axx in ax:\n",
    "    axx.axvline(x=z_st, ls='--', color='b')\n",
    "    axx.set_xlabel('Mixture fraction [-]')\n",
    "\n",
    "ax[1].set_xlim([0, 0.1])\n",
    "    \n",
    "ax[0].set_ylabel('Temperature [K]')\n",
    "ax[1].set_ylabel('Mass fraction [-]')\n",
    "\n",
    "ax[0].legend()\n",
    "custom_lines = [Line2D([0], [0], color='c'),\n",
    "                Line2D([0], [0], color='r'),\n",
    "                Line2D([0], [0], color='m')]\n",
    "ax[1].legend(custom_lines, [r'$\\mathrm{H} \\cdot 10^1$', r'OH', r'$\\mathrm{HO_2} \\cdot 10^2$'], loc='upper right')\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acd64a00-9cb0-4f77-8120-cf504ca8fb76",
   "metadata": {},
   "source": [
    "*Proposed discussion: Temperature and OH mass fraction peaks around the stoichiometric mixture fraction $z_{st}$ where the diffusion flame is located. As expected, temperature, YO, and YOH profiles differ in the value of the maximum. The differences in the maximum values is mainly due to finite rate chemistry effects. Lewis number and preferential diffusion effects also play a role for this highly-diffusive and diffusionally-imbalanced fuel.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a601b8ee-62c3-44da-8aac-5affb34dd355",
   "metadata": {},
   "source": [
    "---\n",
    "**Exercise:** Compute the reduced scalar dissipation rate $\\bar{\\chi}=\\left(\\frac{d z_H}{d x}\\right)^2$ and comment on its trend as a function of strain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1aa0242b-e445-4cc0-8cf8-b9d0db2a59f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls = ['-', ':']\n",
    "fig, ax = plt.subplots(figsize=(5,3.5))\n",
    "axtw = ax.twinx()\n",
    "\n",
    "for i, f in enumerate(f_list):\n",
    "    \n",
    "    # plot mixture fraction\n",
    "    ax.plot(f.flame.grid, z_flames[i], c='k', ls=ls[i], label='%.0f m/s' % v_list[i])\n",
    "\n",
    "    # plot scalar dissipation rate\n",
    "    sdr = np.power(np.gradient(z_flames[i], f.flame.grid), 2)\n",
    "    axtw.plot(f.flame.grid, sdr, c='r', ls=ls[i])\n",
    "\n",
    "#ax[1].set_xlim([0, 0.1])\n",
    "    \n",
    "ax.set_xlabel('Axis [m]')\n",
    "ax.set_ylabel('Mixture fraction [-]')\n",
    "axtw.set_ylabel(r'Reduced scalar diss. rate [$\\mathrm{m^{-2}}$]')\n",
    "\n",
    "ax.legend()\n",
    "custom_lines = [Line2D([0], [0], color='k'),\n",
    "                Line2D([0], [0], color='r')]\n",
    "axtw.legend(custom_lines, [r'$z_H$', r'$\\overline{\\chi}$'], loc='center right')\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0f63abe-4a83-4322-a4a8-d0607a72297f",
   "metadata": {},
   "source": [
    "The mass fraction balance equation in the mixture fraction space may be written as [TNC]\n",
    "$$ \\rho \\frac{\\partial Y_k}{\\partial t}=\\dot{\\omega}_k+\\rho \\mathcal{D}\\left(\\frac{\\partial z}{\\partial x_i} \\frac{\\partial z}{\\partial x_i}\\right) \\frac{\\partial^2 Y_k}{\\partial z^2}=\\dot{\\omega}_k+\\frac{1}{2} \\rho \\chi \\frac{\\partial^2 Y_k}{\\partial z^2} $$\n",
    "where the scalar dissipation rate $\\chi$ has been introduced:\n",
    "$$ \\chi=2 \\mathcal{D}\\left(\\frac{\\partial z}{\\partial x_i} \\frac{\\partial z}{\\partial x_i}\\right) $$\n",
    "The temperature equation can also be recast in the same way:\n",
    "$$ \\rho \\frac{\\partial T}{\\partial t}=\\dot{\\omega}_T+\\frac{1}{2} \\rho \\chi \\frac{\\partial^2 T}{\\partial z^2} $$\n",
    "In these equations, the only term depending on spatial variables is the scalar dissipation rate $\\chi$, which controls mixing. The scalar dissipation rate is directly influenced by strain: then the flame strain increases, $\\chi$ increases. \n",
    "\n",
    "Under steady flamelet assumption, the balance equations reduce to:\n",
    "$$ \\dot{\\omega}_k=-\\frac{1}{2} \\rho \\chi \\frac{\\partial^2 Y_k}{\\partial z^2} $$\n",
    "and\n",
    "$$ \\dot{\\omega}_T=-\\frac{1}{2}  \\rho \\chi  \\frac{\\partial^2 T}{\\partial z^2} $$\n",
    "These equations show that reaction rates for species and temperature depend on $z$ and $\\chi$ only. $\\chi$ is a key quantity in diffusion flame descriptions.\n",
    "\n",
    "\n",
    "The maximum scalar dissipation rate $\\chi$ increases with the strain rate of the flame. From [TNC] Section 3.4.2, for a one-dimensional strained flame with constant density, the scalar dissipation rate is given by:\n",
    "$$ \\chi=2 \\mathcal{D}\\left(\\frac{\\partial z}{\\partial x}\\right)^2=\\frac{a}{\\pi} \\exp \\left(-\\frac{a}{\\mathcal{D}} x^2\\right) $$\n",
    "\n",
    "[TNC] T. Poinsot and D. Veynante, Theoretical and Numerical Combustion.\n",
    "\n",
    "\n",
    "*Proposed discussion: according to the theory, the scalar dissipation rate increases with the strain rate of the flame. $\\chi$ measures the $z$-gradients and the molecular fluxes of species towards the flame. It is directly influenced by strain.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddf077e3-bdba-4ccd-91c3-1095485f786d",
   "metadata": {},
   "source": [
    "---\n",
    "**Exercise:** Compare with the equilibrium solution (infinitely fast chemistry, \"mixed-is-burnt\" model).\n",
    "\n",
    "Let's compute the equilibrium solution for a range of mixture fraction. First, we need to compute the mixing solution. For that, we need the gas state at the boundaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed7dd7bd-b150-4921-a94f-48b249a89a3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute fuel state properties\n",
    "gas.TPX = TIN_F, PRESS, COMP_F\n",
    "H_F = copy.deepcopy(gas.h)\n",
    "YK_F = copy.deepcopy(gas.Y)\n",
    "\n",
    "# compute oxidizer state properties\n",
    "gas.TPX = TIN_OX, PRESS, COMP_OX\n",
    "H_OX = copy.deepcopy(gas.h)\n",
    "YK_OX = copy.deepcopy(gas.Y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05955f44-c6d6-47a5-b940-055539303127",
   "metadata": {},
   "source": [
    "We can then compute the pure mixing quantities for a range of mixture fraction, a bring that mixture to equilibrium."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41dcef3a-374f-4531-968d-4b7f324046e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "z_arr = np.linspace(0, 1, 500)\n",
    "\n",
    "Y_mix = np.zeros([gas.n_species, len(z_arr)])\n",
    "T_mix = np.zeros(len(z_arr))\n",
    "\n",
    "Y_equil = np.zeros([gas.n_species, len(z_arr)])\n",
    "T_equil = np.zeros(len(z_arr))\n",
    "\n",
    "for i, zi in enumerate(z_arr):\n",
    "\n",
    "    # pure mixing\n",
    "    gas.HPY = H_F*zi + H_OX*(1.-zi), PRESS, YK_F*zi + YK_OX*(1.-zi)\n",
    "    Y_mix[:, i] = gas.Y\n",
    "    T_mix[i] = gas.T\n",
    "\n",
    "    # compute the equilibrium gas state\n",
    "    gas.equilibrate('HP')\n",
    "    Y_equil[:, i] = gas.Y\n",
    "    T_equil[i] = gas.T"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37cb45be-1b5f-4af8-a3ac-7c7fe0543472",
   "metadata": {},
   "source": [
    "Let's compare the results of the diffusion flame with the equilibrium gas state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7665e9b9-7733-4089-925c-2887bb92fe0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(ncols=2, figsize=(9,3.5))\n",
    "\n",
    "## diffusion flame\n",
    "ind = -1\n",
    "\n",
    "# plot temperature\n",
    "ax[0].plot(z_flames[ind], f_list[ind].T, c='k', ls=':', label='%.0f m/s' % v_list[ind])\n",
    "\n",
    "# plot species\n",
    "ax[1].plot(z_flames[ind], f_list[ind].Y[gas.species_index('H2'), :], c='b', ls=':')\n",
    "ax[1].plot(z_flames[ind], f_list[ind].Y[gas.species_index('O2'), :], c='g', ls=':')\n",
    "\n",
    "## equilibrium gas state\n",
    "\n",
    "# plot temperature\n",
    "ax[0].plot(z_arr, T_equil, c='k', ls='-', label='equil.')\n",
    "\n",
    "# plot species\n",
    "ax[1].plot(z_arr, Y_equil[gas.species_index('H2'), :], c='b', ls='-')\n",
    "ax[1].plot(z_arr, Y_equil[gas.species_index('O2'), :], c='g', ls='-')\n",
    "\n",
    "for axx in ax:\n",
    "    #axx.axvline(x=z_st, ls='--', color='b')\n",
    "    axx.set_xlabel('Mixture fraction [-]')\n",
    "\n",
    "ax[1].set_xlim([0, 0.15])\n",
    "ax[1].set_ylim([0, 0.1])\n",
    "    \n",
    "ax[0].set_ylabel('Temperature [K]')\n",
    "ax[1].set_ylabel('Mass fraction [-]')\n",
    "\n",
    "ax[0].legend()\n",
    "custom_lines = [Line2D([0], [0], color='b'),\n",
    "                Line2D([0], [0], color='g')]\n",
    "ax[1].legend(custom_lines, [r'$\\mathrm{H_2}$', r'$\\mathrm{O_2}$'], loc='upper right')\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de62ce89-fda2-40ff-bac2-1b48b07e5698",
   "metadata": {},
   "source": [
    "*Proposed discussion: Because of finite rate chemistry and unequal species (preferential diffusion) and $Le \\neq 1$ (thermodiffusive) effects and the additional effect of scalar dissipation rate, the species and temperature profiles differ from the corresponding equilibrium (infinite-rate chemistry, equidiffusive flames) linear profiles.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4484324-cc1c-41a0-8e1e-5794ca3ada38",
   "metadata": {},
   "source": [
    "---\n",
    "**Exercise:** Compute the pure mixing solution and plot the species profiles in mixture fraction space.\n",
    "\n",
    "Note: the pure mixing solution is already computed, see Y_mix and T_mix numpy arrays."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e36e5817-7859-4e1a-bf21-15d38b749479",
   "metadata": {},
   "source": [
    "Let's compare the results of the diffusion flame with the pure mixing gas state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d95e9d58-7e82-4518-93de-419a8f48bcc9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(ncols=2, figsize=(9,3.5))\n",
    "\n",
    "## diffusion flame\n",
    "ind = -1\n",
    "\n",
    "# plot temperature\n",
    "ax[0].plot(z_flames[ind], f_list[ind].T, c='k', ls=':', label='%.0f m/s' % v_list[ind])\n",
    "\n",
    "# plot species\n",
    "ax[1].plot(z_flames[ind], f_list[ind].Y[gas.species_index('H2'), :], c='b', ls=':')\n",
    "ax[1].plot(z_flames[ind], f_list[ind].Y[gas.species_index('O2'), :], c='g', ls=':')\n",
    "\n",
    "## equilibrium gas state\n",
    "\n",
    "# plot temperature\n",
    "ax[0].plot(z_arr, T_mix, c='k', ls='-', label='mix.')\n",
    "\n",
    "# plot species\n",
    "ax[1].plot(z_arr, Y_mix[gas.species_index('H2'), :], c='b', ls='-')\n",
    "ax[1].plot(z_arr, Y_mix[gas.species_index('O2'), :], c='g', ls='-')\n",
    "\n",
    "for axx in ax:\n",
    "    #axx.axvline(x=z_st, ls='--', color='b')\n",
    "    axx.set_xlabel('Mixture fraction [-]')\n",
    "    \n",
    "ax[0].set_ylabel('Temperature [K]')\n",
    "ax[1].set_ylabel('Mass fraction [-]')\n",
    "\n",
    "ax[0].legend()\n",
    "custom_lines = [Line2D([0], [0], color='b'),\n",
    "                Line2D([0], [0], color='g')]\n",
    "ax[1].legend(custom_lines, [r'$\\mathrm{H_2}$', r'$\\mathrm{O_2}$'], loc='upper left')\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e46e6324-cbe9-43f3-8329-8e31c53362dc",
   "metadata": {},
   "source": [
    "*Proposed discussion: The mass fraction profiles are linear connecting the boundary values of the fuel and the oxidizer. Since the fuel and oxidizer streams have equal temperatures, the temperature profile is a straight line at 300 K.*"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
